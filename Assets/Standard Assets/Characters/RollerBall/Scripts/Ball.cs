using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
namespace UnityStandardAssets.Vehicles.Ball
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private float m_MovePower = 5; // The force added to the ball to move it.
        [SerializeField] private bool m_UseTorque = true; // Whether or not to use torque to move the ball.
        [SerializeField] private float m_MaxAngularVelocity = 25; // The maximum velocity the ball can rotate at.
        [SerializeField] private float m_JumpPower = 2; // The force added to the ball when it jumps.

        private const double initialX = -0.2;
        private const double initialY = 5.1;
        private const double initialZ = 0;
        private const int SCORFINAL = 3;

        private const float k_GroundRayLength = 1f; // The length of the ray to check if the ball is grounded.
        private Rigidbody m_Rigidbody;
        private double scoreUser;
        private double scoreOpponent;


        public Text goalText;
        public Text scoreText;
        public Text winText;

        private void Start()
        {
            StreamReader f = new StreamReader("file.txt");
            Double.TryParse(f.ReadLine(), out scoreUser);
            Double.TryParse(f.ReadLine(), out scoreOpponent);
            f.Close();
            m_Rigidbody = GetComponent<Rigidbody>();
            // Set the maximum angular velocity.
            GetComponent<Rigidbody>().maxAngularVelocity = m_MaxAngularVelocity;
            SetText();
        }
        //public void Update()
        //{
        //    if (Input.GetKey(KeyCode.Escape))
        //    {
        //        //Return to menu
        //        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //        SceneManager.UnloadSceneAsync("GameScene");
        //        SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
        //        //SceneManager.UnloadScene("GameScene");

        //    }

        //}
        //public void Update()
        //{
        //    if (Input.GetKey(KeyCode.Escape))
        //    {
        //        //Return to menu
        //        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //        SceneManager.UnloadSceneAsync("GameScene");
        //        SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
        //        //SceneManager.UnloadScene("GameScene");

        //    }

        //}
        //public void Update()
        // {
        //  if (Input.GetKey(KeyCode.Escape))
        //{
        //Return to menu
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        // SceneManager.UnloadSceneAsync("GameScene");
        //  //SceneManager.UnloadScene("GameScene");

        //  }

        //  }
        public void Move(Vector3 moveDirection, bool jump)
        {

            // If using torque to rotate the ball...
            if (m_UseTorque)
            {
                // ... add torque around the axis defined by the move direction.
                m_Rigidbody.AddTorque(new Vector3(moveDirection.z, 0, -moveDirection.x)*m_MovePower);
            }
            else
            {
                // Otherwise add force in the move direction.
                m_Rigidbody.AddForce(moveDirection*m_MovePower);
            }

            // If on the ground and jump is pressed...
            if (Physics.Raycast(transform.position, -Vector3.up, k_GroundRayLength) && jump)
            {
                // ... add force in upwards.
                m_Rigidbody.AddForce(Vector3.up*m_JumpPower, ForceMode.Impulse);
            }
        }

        private void SetText()
        {

            WriteToFile();
            scoreText.text = "You " + scoreUser.ToString() + " - " + scoreOpponent.ToString() + " Opponent";
            
        }

        private void SetTextWin()
        {
            winText.text = "You WON";

            Destroy(gameObject);

            SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
             //   SceneManager.UnloadSceneAsync("GameScene");
            
        }

        private void SetTextLose()
        {
            winText.text = "You LOST";
          
            Destroy(gameObject);
            SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
               // SceneManager.UnloadSceneAsync("GameScene");
            
        }

        private void WriteToFile()
        {
            StreamWriter f = new StreamWriter("file.txt");
            f.WriteLine(scoreUser.ToString());
            f.WriteLine(scoreOpponent.ToString());
            f.Close();
        }

        IEnumerator OnTriggerExit(Collider other)
        {
            //scoreText.text += "ma";
            goalText.text = "Goal!";
            yield return new WaitForSeconds(1);
            if (other.gameObject.tag == "OpponentGoal")
            {
                scoreUser+= 1;
                SetText();

                if (scoreUser == SCORFINAL)
                {
                    goalText.text = "";
                    SetTextWin();
                    yield return new WaitForSeconds(1);
                    //Return to menu
                    SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
                    //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);

                }
                    
            }

            if (other.gameObject.tag == "OurGoal")
            {
                scoreOpponent +=1;
                SetText();

                if (scoreOpponent == SCORFINAL)
                {
                    goalText.text = "";
                    SetTextLose();
                    yield return new WaitForSeconds(1);
                    //Return to menu
                    // Invoke("Update", 2);
                   // SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);

                    //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
                }
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
}
