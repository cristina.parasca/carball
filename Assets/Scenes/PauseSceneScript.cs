﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseSceneScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Resume()
    {
        Time.timeScale =1;
        Destroy(gameObject);
    }
    public void GoScene()
    {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
        Destroy(gameObject);
       // Destroy(SceneManager.GetActiveScene().buildIndex-1);
    }
}
